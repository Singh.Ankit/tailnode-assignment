package com.ankit.tailnodeassignment;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText name, number;
    Button login;

    public static final String MyPREFERENCES = "MyPrefrences";
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = findViewById(R.id.name);
        number = findViewById(R.id.number);
        login = findViewById(R.id.login);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        if (sharedpreferences.getString(Constants.NAME, null) != null) {
            Intent intent = new Intent(MainActivity.this, LocationActivity.class);
            startActivity(intent);
        }

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(name.getText().toString()) || TextUtils.isEmpty(number.getText().toString())) {
                    Toast.makeText(MainActivity.this, "Enter Name and Mobile Number", Toast.LENGTH_LONG).show();
                } else {


                    Intent intent = new Intent(MainActivity.this, LocationActivity.class);
                    startActivity(intent);

                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString(Constants.NAME, name.getText().toString());
                    editor.putString(Constants.NUMBER, number.getText().toString());
                    editor.apply();
                    editor.commit();


                }
            }
        });
    }


}