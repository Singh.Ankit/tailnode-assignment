package com.ankit.tailnodeassignment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class LocationActivity extends AppCompatActivity {

    Button logout, get_location;
    TextView locality;

    FusedLocationProviderClient fusedLocationProviderClient;

    public static final String MyPREFERENCES = "MyPrefrences";
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        logout = findViewById(R.id.logout);
        get_location = findViewById(R.id.get_location);
        locality = findViewById(R.id.location);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        //getLocatoin();
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.clear();
                editor.apply();
                finish();

                Intent intent = new Intent(LocationActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        get_location.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
               if (ActivityCompat.checkSelfPermission(LocationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
                   fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                       @Override
                       public void onComplete(@NonNull Task<Location> task) {
                           Location location = task.getResult();

                           if (location != null) {
                               try {
                                   Geocoder geocoder = new Geocoder(LocationActivity.this, Locale.getDefault());
                                   List<Address> address = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

                                   locality.setText(address.get(0).getLocality());
                               } catch (IOException e) {
                                   e.printStackTrace();
                               }
                           }
                       }
                   });
                } else {
                    ActivityCompat.requestPermissions(LocationActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);
                }
            }
        });
    }


}